$fn=60;

MNut();
MScrew(size=M_size[0][1][0],length=22);
//list_sizes();

// [nominal size, [ diameter, cap diameter, cap height, socket size, socket depth ]]
// http://www.roymech.co.uk/Useful_Tables/Screws/cap_screws.htm
M_size = [ [3, [3,5.5,3,2.5,1.3]],
           [4, [4,7,4,3,2]],
           [5, [5,8.5,5,4,2.7]],
         ];

// [nominal size, [spanner size, bolt diameter, thickness]]
//http://www.fairburyfastener.com/xdims_metric_nuts.htm
M_nut_size = [ [3, [5.5,3,2.4]],
               [4, [7,4,3.2]],
               [5, [8,5,4.7]]
             ];

// [nominal size, [spanner size, bolt diameter, total thickness, spanner thick]]
//http://www.fullermetric.com/products/nut/din985nylon_insert_lock_nut.aspx
M_nylock_size = [ [3, [5.5,3,4,2.4]],
                  [4, [7,4,5,2.9]],
                  [5, [8,5,5,3.2]]
             ];

module MScrew(size=3,length=16) {
    //Unfortunately we have to iterate every row
    for (row = M_size) {
        if (row[0]==size) {
            make_screw(row=row,len=length);
        }
    }
}

module MNut(size=3){
    //Unfortunately we have to iterate every row
    for (row = M_nut_size) {
        if (row[0]==size) {
            make_nut(row=row);
        }
    }    
}

module MNylockNut(size=3){
    //Unfortunately we have to iterate every row
    for (row = M_nylock_size) {
        if (row[0]==size) {
            make_nylock_nut(row=row);
        }
    }    
}

module make_nut(row) {
    difference() {
        cylinder(d=diameter_from_spanner(row[1][0]),h=row[1][2]+.1,$fn=6);    
        translate([0,0,-1])
        cylinder(d=row[1][1], h=row[1][2]+2);
    }
}

module make_nylock_nut(row) {
    //undefined
    //FIXME: needs variation to have spanner and rounded cap sections. Currently just prints thick nut
    echo("make_nylock_nut: UNDEFINED!");
    
    make_nut(row);
}

module make_screw(row,len=16) {
    translate([0,0,-M_size[0][1][2]])
    difference() {
        union() {
            translate([0,0,.1])
            cylinder(d=row[1][0],h=len+row[1][2]-.1);
            cylinder(d=row[1][1],h=row[1][2]);
        }
        
        translate([0,0,-.1])    
        cylinder(d=row[1][3],h=row[1][4]+.1,$fn=6);
    }
}


module list_sizes() {
    echo("screws: [nominal size, [diameter, cap diameter, cap height, socket size]]");
    for (row = M_size) { 
        echo(row);
    }
    
    echo("nuts: [nominal size, [spanner size, bolt diameter, thickness]]");
    for (row = M_nut_size) { 
        echo(row);
    }
    
    echo("nulock nuts: [nominal size, [spanner size, bolt diameter, total thickness, spanner thick]]");
    for (row = M_nylock_size) { 
        echo(row);
    }
    
}

//This is for a hex head nut or wrench so the 
// number of sides of the polygon are always 6
// 
// This valuable when creating hex nuts
// since the cylinder diameter is no the spanner width
// 
// The diameter between edges can be found from spanner width
function diameter_from_spanner(spanner=5.5) = 2*((spanner/2) / cos(180/6));
    
